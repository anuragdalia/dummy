<?php
/**
 * Created by PhpStorm.
 * User: Anurag Dalia
 * Date: 3/9/2016
 * Time: 1:00 AM
 */

logger("logs-url-calls-logger", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", __FILE__, __FUNCTION__, __LINE__);

include_once __DIR__ . '/core/config.php';
$params = explode("/", explode("?", ltrim($_SERVER['REQUEST_URI'], '/'))[0]);

for ($i = 0; $i < 15; $i++) if (!isset($params[$i])) $params[$i] = "";

realiseMotive();
$url_resolution = file_get_contents('routes.json');
url_resolution(json_decode($url_resolution, true), array_reverse($params));
function url_resolution($adr, $req, $data = null)
{
    $top = array_pop($req);
    if ($data == null) $data = array();
    if (is_array($adr)) {
        foreach ($adr as $k => $v) {
            if ($top == $k) {
                return url_resolution($v, $req, $data);
            }
        }

        if (isset($adr["~"])) {
            foreach ($adr["~"] as $k => $v) {
                if (isset($v["*"])) {

                    if ($top == "" && !isset($v["#"]) && !isset($v["$"])) {
                        foreach ($v as $ek => $ev) {
                            if (!in_array($ek, array("*", "#", "$")))
                                $data[$ek] = $ev;
                        }
                        return file_exists(__DIR__ . "/" . $v["*"]) ? include_once __DIR__ . "/" . $v["*"] : false;
                    } else {
                        if (isset($v["#"]) && isset($v["$"])) {
                            if ($v["$"] == "int") $reg = '/^\d+$/';
                            if ($v["$"] == "email") $reg = '/^\d+$/';
                            if (preg_match($reg, $top)) {
                                $data[$v["#"]] = $top;

                                foreach ($v as $ek => $ev) {
                                    if (!in_array($ek, array("*", "#", "$")))
                                        $data[$ek] = $ev;
                                }

                                $t = array_pop($req);
                                array_push($req, $t);
                                if (is_array($v["*"]) && $t != "") {
                                    return url_resolution($v["*"], $req, $data);
                                }

                                if (!is_array($v["*"]))
                                    return file_exists(__DIR__ . "/" . $v["*"]) ? include_once __DIR__ . "/" . $v["*"] : false;
                            }
                        }
                    }
                }
            }
        }

        echo "Error 404";
    }
}

function logger($file, $data, $filee = "", $function = "", $line = "")
{
    $folder = "logs/" . date("y") . "/" . date('m') . "/" . date("d");
    if (!is_dir($folder)) {
        mkdir($folder, 0777, true);
    }
    file_put_contents($folder . "/" . $file, "[$filee][$function][$line] $data\n", FILE_APPEND);
}

function realiseMotive()
{
    //not more than 10 in 3 minutes otherwise die with a notice
}