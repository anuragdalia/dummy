<?php
/**
 * Created by PhpStorm.
 * User: Anurag Dalia
 * Date: 3/9/2016
 * Time: 6:29 AM
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once 'bootstrap.php';

return ConsoleRunner::createHelperSet($em);
