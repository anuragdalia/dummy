<?php
/**
 * Created by PhpStorm.
 * User: Anurag Dalia
 * Date: 3/10/2016
 * Time: 7:27 AM
 */

include_once __DIR__ . '/aes.php';

function mysql_escape_mimic($inp)
{
    if (is_array($inp))
        return array_map(__METHOD__, $inp);

    if (!empty($inp) && is_string($inp)) {
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
    }

    return $inp;
}

if (!function_exists('hash_equals')) {
    function hash_equals($str1, $str2)
    {
        if (strlen($str1) != strlen($str2)) {
            return false;
        } else {
            $res = $str1 ^ $str2;
            $ret = 0;
            for ($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
            return !$ret;
        }
    }
}

class Prooof implements JsonSerializable, Serializable
{

    /** @var AES $aes */
    var $blockSize = 128, $db, $key, $aes;
    var $signature = false, $token = false;
    var $flawless = false;
    var $processedInput = array();


    function __construct($key = null)
    {
        if ($key != null)
            $this->setKey($key);
    }

    function setKey($key)
    {
        $this->key = $key;
        $this->aes = new AES("", $this->key, $this->blockSize);
    }

    private function pad($text, $blockSize = 16)
    {
        $pad = $blockSize - (strlen($text) % $blockSize);
        return $text . str_repeat(chr($pad), $pad);
    }

    private function unPad($text)
    {
        $pad = ord($text{strlen($text) - 1});

        if ($pad > strlen($text)) return $text;

        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return $text;

        return substr($text, 0, -1 * $pad);
    }

    public function decrypt($data)
    {
        $this->aes->setData($data);
        return $this->unPad($this->aes->decrypt());
    }

    public function encrypt($data)
    {
        $this->aes->setData($this->pad($data));
        return $this->aes->encrypt();
    }

    public function cleanInput($input)
    {

        $search = array(
            '/<\scrip[^>]*?>.*?<\/script>/si',   // Strip out javascript
            '/<[\/\!]*?[^<>]*?>/si',            // Strip out HTML tags
            '/<\style[^>]*?>.*?<\/style>/siU',    // Strip style tags properly
            '/<![\s\S]*?--[ \t\n\r]*>/'         // Strip multi-line comments
        );

        $output = preg_replace($search, '', $input);
        return $output;
    }

    public function sanitize($input)
    {
        if (is_array($input)) {
            foreach ($input as $var => $val) {
                $output[$var] = sanitize($val);
            }
        } else {

            $input = stripslashes($input);
            $input = $this->cleanInput($input);
            $output = mysql_escape_mimic($input);
        }
        return $output;
    }

    /**
     * send back a json output
     *
     * @param $code integer
     * @param $msg string
     * @param $load array
     * @param $generateToken boolean
     * @param $callback
     */
    public function __die($code = 0, $msg = "Success", $load = null, $signatureData = null, $generateSignature = false, $generateToken = false, $callback = "")
    {
        $arr = array();

        $arr['code'] = $code;
        $arr['msg'] = $msg;
        if (!is_null($load))
            $arr['load'] = $load;
        if ($generateSignature) {
            $data = $signatureData == null ? $this->getString($load) : $this->getString($signatureData);
            if ($data != "." && $data != '' && $data != null)
                $arr['signature'] = hash_hmac("sha1", $data, $this->key);
        }
        if ($generateToken)
            $token = $this->urlSafe_b64encode(openssl_random_pseudo_bytes(16));
        if ($callback != "") {
            if ($callback($token))
                $arr['token'] = $token;
        }

        logger("outputs",json_encode($arr, JSON_PRETTY_PRINT),__FILE__,__FUNCTION__,__LINE__);
        die(json_encode($arr, JSON_PRETTY_PRINT));
    }

    public function convertKeyToValue($dictionary, $keys)
    {
        foreach ($keys as $key => $value) {
            if (key_exists($value, $dictionary)) {
                $keys[$key] = $dictionary[$value];

            }
        }

        return $keys;
    }

    public function getString($array, $del = "")
    {
        $data = "";

        if (is_array($array)) {
            foreach ($array as $item) {
                if (is_array($item)) {
                    $data .= $del . $this->getString($item);
                } elseif (is_object($item)) {
                    continue;
                } else {
                    $data .= $del . $item;
                }
            }
        } else $data .= $del . $array;

        return $data;
    }

    public function accumulator()
    {
        return func_get_args();
    }

    private function urlSafe_b64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    private function urlSafe_b64decode($string)
    {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    function __toString()
    {
        return "";
    }

    function jsonSerialize()
    {
    }

    function serialize()
    {
    }

    function unserialize($serialized)
    {
    }

    function processInput($compulsoryParameters = null, $optionalParameters = null, $raw_dictionary)
    {
        $dictionary = array();
        foreach ($raw_dictionary as $k => $v) {
            if (is_array($raw_dictionary[$k])) {
                $dictionary[$k] = $this->getString($raw_dictionary[$k]);
            } else
                $dictionary[$k] = $v;
        }

        $this->flawless = false;
        $totalCompulsoryParameters = count($compulsoryParameters);
        $checkedCompulsoryParameters = 0;
        /*$totalOptionalParameters = count($optionalParameters);
        $checkedOptionalParameters = 0;*/
        if ($compulsoryParameters != null) {
            foreach ($compulsoryParameters as $parameter) {

                if (key_exists($parameter['name'], $dictionary) && $dictionary[$parameter['name']] != "") {
                    $checkedCompulsoryParameters++;

                    if($parameter['type']=="json")
                    {
                        if($parameter['content']!=null)
                        {
                            $jsonDictionary = json_decode($dictionary[$parameter['name']],true);
                            foreach ($parameter['content'] as $jsonParam) {
                               if(!isset($jsonParam['compulsory']) || $jsonParam['compulsory']) {
                                   if (key_exists($jsonParam['name'], $jsonDictionary) && $jsonDictionary[$jsonParam['name']] != "") {
                                       $this->processedInput[$parameter['name']][$jsonParam['name']] = $this->setType($jsonParam['type'], $this->sanitize($jsonParam['encrypted'] ? $this->decrypt($jsonDictionary[$jsonParam['name']]) : $jsonDictionary[$jsonParam['name']]));
                                       if ($this->processedInput[$parameter['name']][$jsonParam['name']] == "") {
                                           $this->__die(1404, "Missing Parameter : " . $parameter['name']."[".$jsonParam['name']."]");
                                       }
                                   }
                                   else{
                                   $this->__die(1404, "Missing Parameter : " . $parameter['name']."[".$jsonParam['name']."]");
                                   }
                               }else{

                                    if (key_exists($jsonParam['name'], $jsonDictionary) && $jsonDictionary[$jsonParam['name']] != "") {
                                        $this->processedInput[$parameter['name']][$jsonParam['name']] = $this->setType($jsonParam['type'], $this->sanitize($jsonParam['encrypted'] ? $this->decrypt($jsonDictionary[$jsonParam['name']]) : $jsonDictionary[$jsonParam['name']]));
                                    }
                                    else{
                                        $this->processedInput[$parameter['name']][$jsonParam['name']] = $this->setType($jsonParam['type'], isset($jsonParam['def']) ? $jsonParam['def'] : null);
                                    }

                                }

                            }
                        }
                    }
                    else {

                        $this->processedInput[$parameter['name']] = $this->setType($parameter['type'], $this->sanitize($parameter['encrypted'] ? $this->decrypt($dictionary[$parameter['name']]) : $dictionary[$parameter['name']]));
                        if ($this->processedInput[$parameter['name']] == "") {
                            $this->__die(1404, "Missing Parameter : " . $parameter['name']);
                        }
                    }
                } else {
                    $this->__die(1404, "Missing Parameter : " . $parameter['name']);
                }
            }
        }


        if ($optionalParameters != null) {
            foreach ($optionalParameters as $parameter) {
                if (key_exists($parameter['name'], $dictionary) && !empty($dictionary[$parameter['name']])) {

                    $this->processedInput[$parameter['name']] = $this->setType($parameter['type'], $this->sanitize($parameter['encrypted'] ? $this->decrypt($dictionary[$parameter['name']]) : $dictionary[$parameter['name']]));
                } else {
                    $this->processedInput[$parameter['name']] = $this->setType($parameter['type'], isset($parameter['def']) ? $parameter['def'] : null);
                }

            }
        }

        $this->flawless = ($totalCompulsoryParameters == $checkedCompulsoryParameters);

        return $this;
    }

    function validateSignature($key, $data, $signature = null)
    {

        if ($signature == null) {
            if (!isset($this->processedInput['signature'])) {
                $this->signature = false;
                return $this;
            }
            $signature = $this->processedInput['signature'];
        }

        $signature = $this->getString($signature);

        $string = $this->getString($this->convertKeyToValue($this->processedInput, $data));

        $this->signature = hash_equals(hash_hmac("sha1", $string, $key), $signature);

        if (!$this->signature)
            $this->__die(1501, "Signature Mismatch");

        return $this;
    }

    function validateToken($savedToken, $token)
    {
        $this->token = hash_equals($savedToken, $token);
        if(!$this->token)
            $this->__die(1512,"Invalid Token");
        return $this;
    }

    function getResult($f = false, $s = false, $t = false)
    {
        if (!$f && !$s && !$t)
            return false;
        return (($f ? $this->flawless : true) && ($s ? $this->signature : true) && ($t ? $this->token : true)) ? $this->processedInput : false;
    }

    function setType($type, $value)
    {
        $def = ($value == null);

        if ($type == "int") {
            return $def ? 0 : (preg_match('/^([0-9]*)$/', $value)) ? 0 + $value : 0;
        } elseif ($type == "tel") {
            return $def ? "" : (preg_match('/^(\d{8,12})$/', $value)) ? $value : "";
        } elseif ($type == "loc") {
            return $def ? 0.00 : (preg_match('/^(\-?\d+(\.\d+)?)$/', $value)) ? $value : 0.00;
        } elseif ($type == "date") {
            return $def ? '25/07/1995' : (preg_match('/^((0[1-9])|(1[0-2]))[\/-]((0[1-9])|(1[0-9])|(2[0-9])|(3[0-1]))[\/-](\d{4})$/', $value)) ? $value : '25/07/1995';
        } elseif ($type == "zip") {
            return $def ? "" : (preg_match('/^(\d{5,6}(?:[-\s]\d{4})?)$/', $value)) ? $value : "";
        } elseif ($type == "email") {
            return $def ? "" : (preg_match('/^([a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+)$/', $value)) ? $value : "";
        } elseif ($type == "url") {
            return $def ? "" : (preg_match('/^(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,})$/', $value)) ? $value : "";
        } elseif ($type == "charlimit") {
            return $def ? "" : (preg_match('/^([\w]{1,140})$/', $value)) ? $value : "";
        } elseif ($type == "amount") {
            return $def ? 0 : (preg_match('/^(\d{1,3}(,?\d{3})*(\.\d{1,2})?)$/', $value)) ? $value : 0;
        } elseif ($type == "account") {
            return $def ? "" : (preg_match('/^([anurgdliANURGDLI0-9]){6}$/', $value)) ? $value : "";
        } elseif ($type == "float") {
            return $def ? 0.0 : (preg_match('/^-?(?:\d+|\d*\.\d+)$/', $value)) ? $value : 0.0;
        } elseif ($type == "long") {
            return $def ? "" : (preg_match('/^(\d{9}|\d{10,})$/', $value)) ? $value : "";
        } elseif ($type == "string") {
            return $def ? "" : (preg_match('/^([a-zA-Z0-9 ]*)$/', $value)) ? $value : "";
        } elseif ($type == "word") {
            return $def ? "" : (preg_match('/^([a-zA-Z0-9]*)$/', $value)) ? $value : "";
        } elseif ($type == "doctrine_datetime") {
            return $def ? new \DateTime("-100 day") : (preg_match('/^\d{10,}$/', $value)) ? (new \DateTime())->setTimestamp($value) : new \DateTime("-100 day");
        } elseif ($type == "text") {
            return $def ? "" : (preg_match('/^([a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+\=\-\{\}\[\]\:\"\;\'\<\>\?\,\.\\/ ]*)$/', $value)) ? $value : "";
        } elseif ($type == "address") {
            return $def ? "" : (preg_match('/^([a-zA-Z\d\s\-\,\#\.\+]{0,200})$/', $value)) ? $value : "";
        } elseif ($type == "gender") {
            return $def ? "m" : (preg_match('/^[(m|f)]$/', $value)) ? $value : "m";
        } elseif($type=="imei") {
            if (!preg_match('/^[0-9]{15}$/', $value)) return "";
            $sum = 0;
            for ($i = 0; $i < 14; $i++)
            {
                $num = $value[$i];
                if (($i % 2) != 0)
                {
                    $num = $value[$i] * 2;
                    if ($num > 9)
                    {
                        $num = (string) $num;
                        $num = $num[0] + $num[1];
                    }
                }
                $sum += $num;
            }
            if ((($sum + $value[14]) % 10) != 0) return "";
            return $value;
        }
        else
            return $value;

    }

    function generateSignature($str, $key = false)
    {
        if (!$key)
            return hash_hmac("sha1", $str, $this->key);
        else
            return hash_hmac("sha1", $str, $key);
    }

    /**
     *
     * THE POSSIBLE TYPES THAT CAN BE CHECKED
     * Address
     * BankId
     * CardNumber
     * CardType
     * City
     * Country
     * CustomParam
     * CVV
     * 4DBC
     * Email
     * Mobile
     * MonthNum
     * Name
     * Password
     * PinCode
     * State
     * YearNum
     * Boolean
     * URL
     * MMID
     * Mobile10D
     * OTP
     *
     *
     * @param $regex
     * @param $string
     * @return bool
     */
    function testType($type, $string)
    {
        $regExp = [
            'Address' => '/^[\s\S]{1,255}$/',
            'BankId' => '/^[1-9][0-9]*$/',
            'CardNumber' => '/^[0-9]{15,19}$/',
            'CardType' => '/^[\s\S]{1,255}$/',
            'City' => '/^[\s\S]{1,100}$/',
            'Country' => '/^[\s\S]{1,100}$/',
            'CustomParam' => '/^[0-9a-zA-Z ]{1,20}$/',
            'CVV' => '/^[0-9]{3,4}$/',
            '4DBC' => '/^[0-9]{4}$/',
            'Email' => ' /^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-zA-Z]{2,6}(?=>\.[a-zA-Z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/',
            'Mobile' => '/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1}){0,1}98(\s){0,1}(\-){0,1}(\s){0,1}[1-9]{1}[0-9]{7}$/',
            'MonthNum' => '/^([0-9])|1[0-1]$/',
            'Name' => '/^(?!\s*$)[a-zA-Z .]{1,50}$/',
            'Password' => '/^(?=.*\d)(?=.*[A-Za-z])(?=.*[A-Za-z!@#$%^&*]).{8,16}/',
            'PinCode' => '/^(?!\s*$)[a-zA-Z0-9\-, .] ?([a-zA-Z0-9\-, .]|[a-zA-Z0-9\-, .] )*[a-zA-Z0-9\-, .]{1,7}$/',
            'State' => '/^[\s\S]{1,100}$/',
            'YearNum' => '/^20[1-9][0-9]$/',
            'Boolean"=>/^true|false$/',
            'URL' => '/((([A-Za-z]{3,9}=>(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/',
            'MMID' => '/^(?!0)[a-zA-Z0-9]{7}$/',
            'Mobile10D' => '/^(?!0)[0-9]{10,12}$/',
            'OTP' => '/^(?!0)[0-9]{6}$/'
        ];
        if (preg_match($regExp[$type], $string))
            return true;
        else
            return false;
    }
}
