<?php


use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";
include_once 'config.php';

$isDevMode = true;

//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/xml"),$isDevMode);
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/yml"), $isDevMode);
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/models"), $isDevMode, null, null, false);
$connectionOptions = array(
    'driver' => 'pdo_mysql',
    'host' => DB_HOST,
    'dbname' => DB_NAME,
    'user' => DB_USER,
    'password' => DB_PASSWORD
);

$em = EntityManager::create($connectionOptions, $config);

function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2)
{
    $theta = $longitude1 - $longitude2;
    $miles = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
    $miles = acos($miles);
    $miles = rad2deg($miles);
    $miles = $miles * 60 * 1.1515;
    $feet = $miles * 5280;
    $yards = $feet / 3;
    $kilometers = $miles * 1.609344;
    $meters = $kilometers * 1000;
    return compact('miles', 'feet', 'yards', 'kilometers', 'meters');
}

$generateUniquePin = function ($number) use ($em,&$generateUniquePin)
{
    $arr = array('A','D','G','I','L','N','R','U','1','2','3','4','5','6','7','8','9','0');
    $token = "";
    for ($i = 0; $i < $number; $i ++)
    {
        $index = rand(0, count($arr) - 1);
        $token .= $arr[$index];
    }

    if($em->find("User",$token))
    {
        return $generateUniquePin($number);
    }
    else
    {
        return $token;
    }
};

function os()
{
    $ref=$_REQUEST['ref'];

    //Detect special conditions devices
    $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
    $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
    $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
    $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

    if( $iPod || $iPhone )
    {
        return "iphone";
    }
    else if($iPad)
    {
        return "ipad";
    }
    else if($Android)
    {
        return "android";
    }
    else if($webOS)
    {
        return "web";
    }
    else
    {
        return "unknown";
    }
}

